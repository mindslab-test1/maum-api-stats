import com.google.protobuf.gradle.*
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val grpcVersion = "1.30.0"
//val grpcKotlinVersion = "0.1.3"
val protobufVersion = "3.13.0"
val coroutinesVersion = "1.3.9"

plugins {
    application
    id("org.springframework.boot") version "2.3.2.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    kotlin("jvm") version "1.4.0"
    kotlin("plugin.spring") version "1.4.0"
    kotlin("plugin.jpa") version "1.4.0"
    id("com.google.protobuf") version "0.8.13"
    id("org.jetbrains.kotlin.plugin.allopen") version "1.4.0"
}

group = "ai.maum"
version = "0.0.0"
java.sourceCompatibility = JavaVersion.VERSION_11
application.mainClassName = "ai.maum.statistics.StatisticsApplicationKt"

repositories {
    mavenLocal()
    google()
    jcenter()
    mavenCentral()
    //ojdbc
    maven ( url= "https://code.lds.org/nexus/content/groups/main-repo")
    //lombok
    maven ( url="https://plugins.gradle.org/m2/")
}

sourceSets {
    main {
        java {
            srcDir("src/main/protoGen")
        }
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
    implementation("org.springframework.boot:spring-boot-starter-data-redis")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation(kotlin("stdlib"))
    implementation("javax.annotation:javax.annotation-api:1.2")

    implementation("io.grpc:grpc-services")
    implementation("io.grpc:grpc-netty")
    implementation("io.github.lognet:grpc-spring-boot-starter:3.5.5")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
    runtimeOnly("io.grpc:grpc-netty-shaded:$grpcVersion")

    runtimeOnly("com.h2database:h2")
    implementation ("com.oracle:ojdbc6:11.2.0.3")
    compileOnly("org.projectlombok:lombok:1.18.6") {
        annotationProcessor("org.projectlombok:lombok")
    }

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:$protobufVersion"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion"
        }
    }
    generateProtoTasks {
        ofSourceSet("main").forEach {
            it.plugins {
                id("grpc") {
                    outputSubDir = "protoGen"
                }
            }
        }
    }
    generatedFilesBaseDir = "$projectDir/src/"
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}
