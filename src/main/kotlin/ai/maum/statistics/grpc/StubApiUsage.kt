package ai.maum.statistics.grpc

import ai.maum.api.stats.protobuf.SaveAPIUsageRequest
import ai.maum.api.stats.protobuf.StatsServerGrpc
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import org.slf4j.LoggerFactory
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import java.util.concurrent.TimeUnit
import kotlin.jvm.Throws

//@Component
class StubApiUsage : ApplicationRunner {
    private val logger = LoggerFactory.getLogger(StubApiUsage::class.java)

    private val producerIp = "localhost"
    private val producerPort = 6565

    private val channel: ManagedChannel

    private val blockingStub: StatsServerGrpc.StatsServerBlockingStub
    private val asyncStub: StatsServerGrpc.StatsServerStub

    init {
        try {
            val channelBuilder = ManagedChannelBuilder.forAddress(producerIp, producerPort).usePlaintext()
            channel = channelBuilder.build()

            blockingStub = StatsServerGrpc.newBlockingStub(channel)
            asyncStub = StatsServerGrpc.newStub(channel)
        } catch (e: Exception) {
            logger.error(e.toString())
            throw Exception("Failed to create GrpcTtsEndpoint object: Failed to create channel or stub.")
        }
    }

    @Throws(InterruptedException::class)
    fun shutdown() {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS)
    }

    override fun run(args: ApplicationArguments?) {
        var request = SaveAPIUsageRequest.newBuilder()
                .setApiId("test")
                .setElapsedTime(1L)
                .setEngine("testEngine")
                .setRequestAosPackage("testAosPackage")
                .setRequestIosUrlScheme("testIosUrlScheme")
                .setRequestApplicationName("testApplicationName")
                .setRequestHost("testHost")
                .setRequestMethod("testMethod")
                .setRequestPayloadSize(64L)
                .setRequestSize(128L)
                .setRequestTime(1234L)
                .setRequestContentType("testContentType")
                .setPathAbsolute("testPath/test")
                .setResponsePayloadSize(16L)
                .setResponseSize(32L)
                .setResponseStatusCode(200L)
                .setResponseTime(1235L)
                .setResponseContentType("testContentType")
                .setSuccess(true)
                .setUuid("testUUID")
                .build()

        blockingStub.saveApiUsage(request)

        logger.warn("HOJHEWIFOHWEO")

        shutdown()
    }
}
