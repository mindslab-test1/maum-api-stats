package ai.maum.statistics.grpc

import ai.maum.api.stats.protobuf.SaveAPIUsageRequest
import ai.maum.api.stats.protobuf.SaveAPIUsageResponse
import ai.maum.api.stats.protobuf.StatsServerGrpc
import ai.maum.statistics.jpa.ApiUsage
import ai.maum.statistics.jpa.ApiUsageRepository
import io.grpc.stub.StreamObserver
import org.lognet.springboot.grpc.GRpcService
import org.slf4j.LoggerFactory
import java.time.Instant

@GRpcService
class StatsGrpcService (
    private val apiUsageRepository: ApiUsageRepository
) : StatsServerGrpc.StatsServerImplBase() {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun saveApiUsage(request: SaveAPIUsageRequest, responseObserver: StreamObserver<SaveAPIUsageResponse>) {
        logger.info(request.toString())

        val newApiUsage = ApiUsage()
        newApiUsage.uuid = request.uuid
        newApiUsage.apiId = request.apiId
        newApiUsage.elapsedTime = request.elapsedTime
        newApiUsage.engine = request.engine
        newApiUsage.resource = request.pathAbsolute
        newApiUsage.requestHost = request.requestHost
        newApiUsage.requestContentType = request.requestContentType
        newApiUsage.requestMethod = request.requestMethod
        newApiUsage.requestTime = Instant.ofEpochMilli(request.requestTime)
        newApiUsage.requestPayloadSize = request.requestPayloadSize
        newApiUsage.requestAosPackage = request.requestAosPackage
        newApiUsage.requestIosUrlScheme = request.requestIosUrlScheme
        newApiUsage.requestApplicationName = request.requestApplicationName
        newApiUsage.responseContentType = request.responseContentType
        newApiUsage.responsePayloadSize = request.responsePayloadSize
        newApiUsage.responseStatusCode = request.responseStatusCode
        newApiUsage.responseSize = request.responseSize
        newApiUsage.responseTime = Instant.ofEpochMilli(request.responseTime)
        newApiUsage.success = request.success

        val apiUsage = apiUsageRepository.save(newApiUsage)

        val responseBuilder = SaveAPIUsageResponse.newBuilder()
            .setMessage("success")

        responseObserver.onNext(responseBuilder.build())
        responseObserver.onCompleted()
    }
}
