package ai.maum.statistics.jpa

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.Instant
import javax.persistence.*

@AllOpen
@Entity
@EntityListeners(AuditingEntityListener::class)
class ApiUsage {
    /*
     * Row ID
     */
    @Id
    @SequenceGenerator(
            name = "APIUSAGE_SEQ_GEN",
            sequenceName = "APIUSAGE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APIUSGAE_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = true)
    var uuid: String? = null

    /*
     * API ID for user identification
     */
    @Column(nullable = true)
    var apiId: String? = null

    /*
     * Relative resource identification
     */
    @Column(nullable = true, name = "PATH_ABSOLUTE")
    var resource: String? = null

    /*
     * Corresponding engine of request
     */
    @Column(nullable = true)
    var engine: String? = null

    @Column(nullable = true)
    var requestSize: Long? = null

    @Column(nullable = true)
    var requestPayloadSize: Long? = null

    @Column(nullable = true)
    var requestContentType: String? = null

    @Column(nullable = true)
    var requestMethod: String? = null

    @Column(nullable = true)
    var requestHost: String? = null

    /*
     * Device dependent information
     */
    @Column(nullable = true)
    var requestAosPackage: String? = null

    @Column(nullable = true)
    var requestIosUrlScheme: String? = null

    @Column(nullable = true)
    var requestApplicationName: String? = null

    @Column(nullable = true)
    var responseSize: Long? = null

    @Column(nullable = true)
    var responsePayloadSize: Long? = null

    @Column(nullable = true)
    var responseContentType: String? = null

    @Column(nullable = true)
    var responseStatusCode: Long? = null

    /* Timing values */
    @Column(nullable = true)
    var requestTime: Instant? = null

    @Column(nullable = true)
    var responseTime: Instant? = null

    @Column(nullable = true)
    var elapsedTime: Long? = null // in milliseconds

    @CreatedDate
    @Column(updatable = false)
    var created: Instant? = null

    @Column(nullable = true)
    var success: Boolean? = null
}
