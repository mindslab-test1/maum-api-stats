package ai.maum.statistics.jpa

import org.springframework.data.jpa.repository.JpaRepository

interface ApiUsageRepository : JpaRepository<ApiUsage, Long> {
}