package ai.maum.statistics.mongo

import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component

//@Component
class MongoRunner(
        var mongoTemplate: MongoTemplate
) : ApplicationRunner {
    override fun run(args: ApplicationArguments) {
        var account = Account()
        account.email = "b@b.c"
        account.name = "bob"

        account = mongoTemplate.insert(account)
    }
}
