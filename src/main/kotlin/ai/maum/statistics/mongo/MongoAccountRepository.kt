package ai.maum.statistics.mongo

import org.springframework.data.mongodb.repository.MongoRepository

interface MongoAccountRepository : MongoRepository<Account, Long> {
}