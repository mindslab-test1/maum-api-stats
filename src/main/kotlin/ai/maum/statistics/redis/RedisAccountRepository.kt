package ai.maum.statistics.redis

import org.springframework.data.keyvalue.repository.KeyValueRepository

interface RedisAccountRepository : KeyValueRepository<Account, String> {
}