package ai.maum.statistics.redis

import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

//@Component
class RedisRunner(
        var redisTemplate: StringRedisTemplate,
        var redisAccountRepository: RedisAccountRepository
) : ApplicationRunner {
    override fun run(args: ApplicationArguments) {
        val values = redisTemplate.opsForValue()
        values.set("name", "test");
        values.set("framework", "spring");
        values.set("message", "hello world");

        var account = Account()
        account.email = "a@b.c"
        account.name = "alice"

        account = redisAccountRepository.save(account)

        val found = redisAccountRepository.findByIdOrNull(account.id!!)
        println(found!!.email!!)
    }
}
