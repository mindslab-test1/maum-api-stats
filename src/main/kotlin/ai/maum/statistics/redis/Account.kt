package ai.maum.statistics.redis

import org.springframework.data.redis.core.RedisHash
import javax.persistence.Id

@RedisHash("accounts")
class Account {
    @Id
    var id: String? = null

    var name: String? = null
    var email: String? = null
}