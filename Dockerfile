FROM openjdk:11.0.8-jre-slim
COPY build/libs/*.jar maum-stats-for-poc.jar

ENTRYPOINT ["java","-jar","/maum-stats-for-poc.jar"]
